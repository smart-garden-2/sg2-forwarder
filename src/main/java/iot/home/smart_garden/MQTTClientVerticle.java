package iot.home.smart_garden;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.mqtt.MqttClient;
import iot.home.smart_garden.common.Topics;
import iot.home.smart_garden.database.DBWriter;
import iot.home.smart_garden.database.InfluxDBWriter;
import static iot.home.smart_garden.common.Topics.*;

public class MQTTClientVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) {
    MqttClient client = MqttClient.create(vertx);

    String token = "VEpoAeFohZCwp4Xo5PfXcfopAi";  // admin token of influxdb
    String org = "home";
    String bucket = "smart_garden";
    String broker = "mosquitto"; // Work thanks to the DNS service of Docker

    DBWriter dbWriter = new InfluxDBWriter(org, bucket, token);
    dbWriter.connect();

    client.connect(1883, broker, con -> {
      client.publishHandler(s -> {
        String topic = s.topicName();
        if(topic.equals(state.getText()) ||
            topic.equals(wateringState.getText())) {
          dbWriter.writeStateData(
            s.payload().toString(),
            Topics.valueOf(
              topic.substring(topic.lastIndexOf("/") + 1)));
        } else {
          dbWriter.writeTelemetryData(
            s.payload().toString(),
            Topics.valueOf(
              topic.substring(topic.lastIndexOf("/") + 1)));
        }
      });
      client.subscribe(telemetry.getText(), 0);
      client.subscribe(tankLevel.getText(), 0);
      client.subscribe(state.getText(), 0);
      client.subscribe(wateringState.getText(), 0);

      if (con.succeeded()) {
        startPromise.complete();
        System.out.println("MQTT client started");
      } else {
        startPromise.fail(con.cause());
      }
    });
  }
}
