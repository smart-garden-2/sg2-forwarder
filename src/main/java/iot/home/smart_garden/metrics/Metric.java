package iot.home.smart_garden.metrics;

public class Metric {
  private String name;
  private double value;

  public String getName() {
    return name;
  }

  public double getValue() {
    return value;
  }
}
