package iot.home.smart_garden.database;

import com.google.gson.Gson;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.WritePrecision;
import iot.home.smart_garden.common.Topics;
import iot.home.smart_garden.metrics.Metric;
import iot.home.smart_garden.metrics.State;
import iot.home.smart_garden.metrics.Telemetry;

public class InfluxDBWriter implements DBWriter {

  Gson g;
  InfluxDBClient influxDBClient;
  String token;
  String org;
  String bucket;

  public InfluxDBWriter(String org, String bucket, String token) {
    this.org = org;
    this.bucket = bucket;
    this.token = token;
    g = new Gson();
  }

  private <T> T jsonToLineProtocol(String jsonDoc, Class<T> inputClass) {
    return g.fromJson(jsonDoc, inputClass);
  }

  @Override
  public void writeTelemetryData(String jsonDoc, Topics topic) {
    Telemetry telemetry = jsonToLineProtocol(jsonDoc, Telemetry.class);
    for (Metric m : telemetry.getMetrics()) {
      try (WriteApi writeApi = influxDBClient.makeWriteApi()) {
        writeApi.writeRecord(WritePrecision.S,
          m.getName() + " " + "value=" + m.getValue() +
            " " + telemetry.getTimestamp());
      }
    }
  }

  @Override
  public void writeStateData(String jsonDoc, Topics topic) {
    State state = jsonToLineProtocol(jsonDoc, State.class);
    try (WriteApi writeApi = influxDBClient.makeWriteApi()) {
      writeApi.writeRecord(WritePrecision.S,
        state.getName() + " " + "value=" + state.getState() +
          " " + state.getTimestamp());
    }
  }

  @Override
  public void connect() {
    this.influxDBClient =
      InfluxDBClientFactory.create(
        "http://influxdb:8086",
        token.toCharArray(),
        org,
        bucket);
  }

  @Override
  public void disconnect() {
    influxDBClient.close();
  }
}
