# 1st Docker build stage: build the project with Gradle
FROM gradle:7.4.2-jdk18 as builder
WORKDIR /project
COPY . /project/
RUN gradle assemble --no-daemon

# 2nd Docker build stage: copy builder output and configure entry point
FROM openjdk:18.0.1-jdk
ENV APP_DIR /application
ENV APP_FILE container-uber-jar.jar

WORKDIR $APP_DIR
COPY --from=builder /project/build/libs/*-fat.jar $APP_DIR/$APP_FILE

ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $APP_FILE"]
